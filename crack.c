#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>

#include "md5.h"

//#define NUM_HASHES 3000000 Defined in makefile
#ifndef NUM_HASHES
exit(5);
#endif

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
	// Hash the guess using MD5
	// Compare the two hashes
	// Free any malloc'd memory
	
	int result;
	char *guess_hash;
	if (!(guess_hash = malloc(sizeof(char) * HASH_LEN))) {
		fprintf(stderr, "Allocation Error\n");
		exit(5);
	}
	
	guess_hash = md5(guess, strlen(guess));
	
	result = strncmp(hash, guess_hash, sizeof(char) * HASH_LEN);
	
	free(guess_hash);
	return !result;

	return 0;
}

// TODO
// Read in the hash file and return the array of strings.
char **read_hashes(char *filename)
{
	FILE *file = NULL;
	
	if (!(file = fopen(filename, "r"))) {
		fprintf(stderr, "Couldn't open file: %s\n", filename);
		exit(5);
	}

	struct stat fileinfo;
	stat(filename, &fileinfo);
	int filesize = fileinfo.st_size;

	char *raw;

	if (!(raw = malloc((filesize + 1) * sizeof(char)))) {
		fprintf(stderr, "Memory allocation error\n");
		exit(6);
	}

	fread(raw, 1, filesize, file);
	fclose(file);
	
	int lines = 0;
	for (int ii = 0; ii < filesize; ii++) {
		if ('\n' == raw[ii]) {
			raw[ii] = '\0';
			lines++;
		}
	}

	char **lineptr;
	if (!(lineptr = malloc(lines * sizeof(char *)))) {
		fprintf(stderr, "Memory allocation error\n");
		exit(7);
	}
	
	int jj = 0;

	for (int ii = 0; ii < filesize - 1; ii++) {
		if ('\0' == raw[ii] && (ii + 1) < filesize - 1) {
			lineptr[jj++] = &raw[ii + 1];
		}
	}
		
	return lineptr;
}


// TODO
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.
char **read_dict(char *filename)
{
	FILE *file = NULL;

	char *string, c;
	int lines = 0, ii = 0;
	size_t size = 0;
	ssize_t ssize;

	file = fopen(filename, "r");
	
	// num of lines for lineptr
	while ((c = fgetc(file)) != EOF) {
		if ('\n' == c) {
			lines++;
		}
	}

	file = fopen(filename, "r");

	char **lineptr = malloc(sizeof(char *) * lines);
	char *data = malloc(sizeof(char) * (PASS_LEN + HASH_LEN) * NUM_HASHES);
	
	string = malloc(sizeof(char) * PASS_LEN);

	// each line gets malloced new memory for hash and password
	while ((ssize = getline(&string, &size, file)) != -1) {
		lineptr[ii] = &data[ii * (PASS_LEN + HASH_LEN)];
		string[strlen(string) - 1] = '\0';
		sprintf(lineptr[ii], "%s:%s", md5(string, strlen(string)), string);
		ii++;
	}

	fclose(file);
	
	return lineptr;
}

int compare (void *a, void *b)
{
	char *hash = malloc(sizeof(char) * HASH_LEN);
	strncpy(hash, (char *)b, HASH_LEN - 1);
	return strncmp((char *)a, hash, HASH_LEN - 1);
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);

    // TODO: Read the dictionary file into an array of strings
    char **dict = read_dict(argv[2]);
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    qsort(dict[0], NUM_HASHES, sizeof(char) * (PASS_LEN + HASH_LEN), &compare);

    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    int ii = 0;
    char *entry = NULL;
    while (ii < NUM_HASHES - 1) {
	    entry = bsearch(hashes[ii], dict[0], NUM_HASHES,
	    		    sizeof(char) * (HASH_LEN + PASS_LEN), &compare);
	    if (entry) {
		    entry = &entry[HASH_LEN];
		    printf("%s\n", entry);
	    }
	    ii++;
    }

    free(hashes);
    free(dict);
}
